import { Injectable } from '@angular/core';
// import { Observable } from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

interface Mail {
  to?: string;
  subject?: string;
  message: string;
  from?: string;
}

@Injectable({
  providedIn: 'root'
})

export class MailService {

  constructor(private http: HttpClient) { }

  sendMail(mail: Mail): Observable<any> {    // todo change any
    const options = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
      })
    };

    return this.http.post(
        'http://1468910.w3tims00.web.hosting-test.net/',
        JSON.stringify(mail),
        options
    );
  }
}
