import {Component, OnInit} from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MailService } from '../../sevices/mail.service';

@Component({
    selector: 'app-contact-form',
    templateUrl: 'contact-form.component.html',
    styleUrls: ['contact-form.component.scss']
})

export class ContactFormComponent implements OnInit {

    constructor(private mailService: MailService) {}

    contactForm = new FormGroup({
        email: new FormControl(''),
        message: new FormControl(''),
    });

    isStarted = false;
    isSent = false;

    sendMail() {
        console.log('starting func!');
        this.isStarted = true;
        this.contactForm.disable();
        this.mailService.sendMail({
            to: 'w3tims@gmail.com',
            subject: 'CodeSurf.notification',
            message:
                `Hey! Someone sent you a message from CodeSurf!
                from: ${this.contactForm.controls["email"].value as string}
                message:  ${this.contactForm.controls["message"].value as string }`
        }).subscribe(
            (answer: {result: 'Success' | 'Error'}) => {
                if (answer.result === 'Success' ) {
                    this.isSent = true;
                }
            }, (err) => {
                console.log('error: ', err);
            }
        );
    }

    ngOnInit() {
    }
}
