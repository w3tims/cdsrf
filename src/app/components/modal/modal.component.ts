import {Component, OnDestroy, OnInit, Output, EventEmitter, Input} from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit, OnDestroy {

  @Input() Title: string;
  @Input() SubTitle: string;
  @Output() onClose = new EventEmitter();

  constructor() { }

  ngOnInit() {
    // document.body.style.overflow = "hidden";
    window.addEventListener( 'wheel', this.blockEvent, false );
  }

  blockEvent($e: Event) {
    $e.preventDefault();
  }

  ngOnDestroy() {
    // document.body.style.overflow = "auto";
    window.removeEventListener( 'wheel', this.blockEvent, false );
  }

  close() {
    this.onClose.emit();
  }
}
