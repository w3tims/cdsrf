import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-scroll4',
  templateUrl: './scroll4.component.html',
  styleUrls: ['./scroll4.component.scss']
})
export class Scroll4Component implements OnInit {

  constructor() { }

  activeT = 1;
  // set number of testimonials:
  numberOfT = 3;

  ngOnInit() {

  }

  next() {
    if (this.activeT < this.numberOfT) {
      this.activeT++;
    } else {
      this.activeT = 1;
    }

    console.log(this.activeT);
  }

  prev() {
    if (this.activeT > 1) {
      this.activeT--;
    } else {
      this.activeT = this.numberOfT;
    }

    console.log(this.activeT);

  }

}
