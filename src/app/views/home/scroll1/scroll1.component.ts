import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-scroll1',
  templateUrl: './scroll1.component.html',
  styleUrls: ['./scroll1.component.scss']
})
export class Scroll1Component implements OnInit {

  isModalOpen = false;

  constructor() { }

  ngOnInit() {
  }

  openModal() {
    this.isModalOpen = true;
  }

  closeModal() {
    this.isModalOpen = false;
  }
}
