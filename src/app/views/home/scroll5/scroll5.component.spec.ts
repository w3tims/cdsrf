import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Scroll5Component } from './scroll5.component';

describe('Scroll5Component', () => {
  let component: Scroll5Component;
  let fixture: ComponentFixture<Scroll5Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Scroll5Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Scroll5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
