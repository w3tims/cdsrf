import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { HomeComponent } from './views/home/home.component';
import { StudioComponent } from './views/studio/studio.component';
import { ServicesComponent } from './views/services/services.component';
import { ContactComponent } from './views/contact/contact.component';
import { Scroll1Component } from './views/home/scroll1/scroll1.component';
import { Scroll2Component } from './views/home/scroll2/scroll2.component';
import { Scroll3Component } from './views/home/scroll3/scroll3.component';
import { Scroll4Component } from './views/home/scroll4/scroll4.component';
import { Scroll5Component } from './views/home/scroll5/scroll5.component';
import { ContactFormComponent } from './components/contact-form/contact-form.component';

import { ModalComponent } from './components/modal/modal.component';
import { MailService } from './sevices/mail.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    StudioComponent,
    ServicesComponent,
    ContactComponent,
    Scroll1Component,
    Scroll2Component,
    Scroll3Component,
    Scroll4Component,
    Scroll5Component,
    ContactFormComponent,
    ModalComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    MailService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
